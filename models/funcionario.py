from dataclasses import dataclass
from uuid import uuid4
from flask import jsonify

from tabulate import tabulate

@dataclass
class Funcionario:
    id: str
    email: str
    senha: str
    confirmacao_senha: str
    nome: str
    idade: int
    cpf: str
    funcao: str
    
    funcionarios = []
    
    @classmethod
    def cadastra_funcionario(cls, funcionario: 'Funcionario'):
        funcionarios = cls.funcionarios
        # Adiciona um id único
        funcionario.id = str(uuid4())
        # Adiciona o ciclista à lista
        funcionarios.append(funcionario)
        response_data = {
                        "id": funcionario.id,
                        "email": funcionario.email,
                        "senha": funcionario.senha,
                        "confirmacao_senha": funcionario.confirmacao_senha,
                        "nome": funcionario.nome,
                        "idade": funcionario.idade,
                        "cpf": funcionario.cpf,
                        "funcao": funcionario.funcao
                    }
        return response_data  

    
    
    def retorna_funcionario(funcionario_id):
        ###consertar erro
        for f in Funcionario.funcionarios:
            if f.id == funcionario_id:
                response_data = {
                        "id": f.id,
                        "email": f.email,
                        "senha": f.senha,
                        "confirmacao_senha": f.confirmacao_senha,
                        "nome": f.nome,
                        "idade": f.idade,
                        "cpf": f.cpf,
                        "funcao": f.funcao
                    }
                return response_data  
            else:
                return "Dados inválidos"

    @classmethod
    def altera_funcionario(cls, id, **dados_atualizados):
        # Encontrar o ciclista pelo ID
        for f in cls.funcionarios:
            if f.id == id:
                # Atualizar apenas os campos fornecidos no JSON
                for campo, valor in dados_atualizados.items():
                    setattr(f, campo, valor)

                response_data = {
                        "id": f.id,
                        "email": f.email,
                        "senha": f.senha,
                        "confirmacao_senha": f.confirmacao_senha,
                        "nome": f.nome,
                        "idade": f.idade,
                        "cpf": f.cpf,
                        "funcao": f.funcao
                    }
                return response_data  
            else:
                return "Dados inválidos"
    
    @classmethod
    def remove_funcionario(cls, funcionario_id):
        # Encontrar o funcionário pelo ID e remover
        for f in cls.funcionarios:
            if f.id == funcionario_id:
                cls.funcionarios.remove(f)
                return "Dados removidos"
        
        return "Não encontrado"

    @staticmethod
    def imprime_funcionarios():
        headers = ["ID", "Nome", "Idade", "CPF", "E-mail", "Função"]
        data = [(f.id, f.nome, f.idade, f.cpf, f.cpf, f.email, f.funcao) for f in Funcionario.funcionarios]

        table = tabulate(data, headers=headers, tablefmt="pretty")
        print(table)
        return table