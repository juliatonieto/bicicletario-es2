from dataclasses import dataclass
from datetime import datetime
import json
from uuid import uuid4
from flask import jsonify
from tabulate import tabulate
import requests

@dataclass
class Passaporte:
    numero: str
    validade: str
    pais: str
    
@dataclass
class MeioDePagamento:
    nome_titular: str
    numero: str
    validade: str
    cvv: str
   
@dataclass
class Ciclista:
    id: str
    nome: str
    nascimento: str
    cpf: str
    passaporte: Passaporte
    nacionalidade: str
    email: str
    url_foto_documento: str
    senha: str
    meio_de_pagamento: MeioDePagamento
    ativo: bool = False
     
    ciclistas = []
         
    
    @classmethod
    def cadastra_ciclista(cls, ciclista: 'Ciclista'):
            ciclistas = cls.ciclistas

            if Ciclista.busca_email(ciclista.email):
                return {
                    "codigo": "E001",
                    "mensagem": "O e-mail informado já está cadastrado. Não foi possível cadastrar o ciclista."
                }

            # Adiciona um id único
            ciclista.id = str(uuid4())
            # Adiciona o ciclista à lista
            ciclistas.append(ciclista)
            ciclista.valida_cc(ciclista.meio_de_pagamento)
            mensagem = "O email foi enviado e que está aguardando confirmação."
            Ciclista.envia_email(ciclista.email, mensagem)

            # Monta e retorna o objeto JSON em caso de sucesso
            response_data = {
                "id": ciclista.id,
                "nome": ciclista.nome,
                "nascimento": ciclista.nascimento,
                "cpf": ciclista.cpf,
                "passaporte": ciclista.passaporte,
                "nacionalidade": ciclista.nacionalidade,
                "email": ciclista.email,
                "urlFotoDocumento": ciclista.url_foto_documento,
                "meio_de_pagamento": ciclista.meio_de_pagamento  
            }

            return response_data
     
  
    def busca_email(email):
        ciclistas = Ciclista.ciclistas
        res = any(c.email == email for c in ciclistas)
        return res

    def envia_email(email, mensagem):
        resposta = requests.post("https://externo-vadebike.onrender.com/enviaremail", {
            'assunto': 'vaDeBike',
            'email': f'{email}',
            'mensagem': f'{mensagem}'
        })
       
        if resposta.status_code == 200:
            mensagem = {'E-mail enviado.'}
        else:
            mensagem = {'Não foi possível enviar o e-mail.'}

        print(mensagem)
        return mensagem
       
    @classmethod
    def valida_cc(cls, meio_de_pagamento: 'MeioDePagamento') :
        meio_de_pagamento = MeioDePagamento(**meio_de_pagamento)
        resposta = requests.post("https://externo-vadebike.onrender.com/validaCartaoDeCredito", {
        "nomeTitular": f'{meio_de_pagamento.nome_titular}',
        "numero": f'{meio_de_pagamento.numero}',
        "validade": f'{meio_de_pagamento.validade}',
        "cvv": f'{meio_de_pagamento.cvv}'
        })
        if resposta.status_code == 200:
            mensagem = {'Cartão validado'}
        else:
            mensagem = {'Cartão recusado ou inválido'}
        print(mensagem)
        return mensagem
  
  
    def ativa_ciclista(id):
        for c in Ciclista.ciclistas:
            if c.id == id:
                c.ativo = True
                response_data = {
                    "id": c.id,
                    "nome": c.nome,
                    "nascimento": c.nascimento,
                    "cpf": c.cpf,
                    "passaporte": c.passaporte,
                    "nacionalidade": c.nacionalidade,
                    "email": c.email,
                    "urlFotoDocumento": c.url_foto_documento,
                    "meio_de_pagamento": c.meio_de_pagamento  
                    }
            return response_data

        
    def retorna_ciclista(ciclista_id):
        for c in Ciclista.ciclistas:
            if c.id == ciclista_id:
                response_data = {
                    "id": c.id,
                    "nome": c.nome,
                    "nascimento": c.nascimento,
                    "cpf": c.cpf,
                    "passaporte": c.passaporte,
                    "nacionalidade": c.nacionalidade,
                    "email": c.email,
                    "urlFotoDocumento": c.url_foto_documento
                }
                return response_data 
        
        return {"codigo": "404", "mensagem": "Ciclista não encontrado."}


            
    @classmethod
    def altera_ciclista(cls, id, **dados_atualizados):
        # Encontrar o ciclista pelo ID
        for ciclista in cls.ciclistas:
            if ciclista.id == id:
                # Atualizar apenas os campos fornecidos no JSON
                for campo, valor in dados_atualizados.items():
                    setattr(ciclista, campo, valor)
                mensagem = "Seus dados foram alterados com sucesso."
                ciclista.envia_email(ciclista.email, mensagem)
                response_data = {
                        "id": ciclista.id,
                        "nome": ciclista.nome,
                        "nascimento": ciclista.nascimento,
                        "cpf": ciclista.cpf,
                        "passaporte": ciclista.passaporte,
                        "nacionalidade": ciclista.nacionalidade,
                        "email": ciclista.email,
                        "urlFotoDocumento": ciclista.url_foto_documento,
                        "meio_de_pagamento": ciclista.meio_de_pagamento  
                    }
                 
            return {"codigo": "200", "mensagem": response_data}
            
        return {"codigo": "500", "mensagem":  "Ciclista não encontrado"}
    
    @classmethod
    def altera_cc(cls, id, **dados_meio_pagamento):
        # Encontrar o ciclista pelo ID
        for ciclista in cls.ciclistas:
            if ciclista.id == id:
                # Atualizar apenas os campos fornecidos
                for campo, valor in dados_meio_pagamento.items():
                    setattr(ciclista.meio_de_pagamento, campo, valor)
                
                ciclista.valida_cc(ciclista.meio_de_pagamento)
                mensagem = "Os dados de pagamento foram alterados."
                ciclista.envia_email(ciclista.email, mensagem)

                response_data = {
                        "meio_de_pagamento": ciclista.meio_de_pagamento  
                    }
                return response_data
            
        return "Dados inválidos"


    def permite_aluguel(self):
    # Verifica se o ciclista já alugou uma bicicleta
        return not bool(self.bicicleta_alugada)

    def bicicleta_alugada(self):
    # Retorna a bicicleta alugada ou vazio se não houver uma
            return self.bicicleta_alugada

    def retorna_cc(ciclista_id):
        for c in Ciclista.ciclistas:
            if c.id == ciclista_id:
                response_data = {
                    "meio_de_pagamento": c.meio_de_pagamento  
                    }
                return response_data        
        else:
            return "Dados inválidos"
    
    def podeAlugar(id_ciclista, numero_bicicleta):
        # Implemente a lógica real de verificação aqui
        # Pode retornar um dicionário com a estrutura: {'status': 200, 'id': 'xyz'} se tudo estiver correto
        return {'status': 200, 'id': 'xyz'}
    


    def aluga_bicicleta(id_tranca, id_ciclista, valor_cobranca, email):
            # Passo 1: Validar o número da tranca
            base_url = 'https://equipamento.onrender.com/tranca'

            # ID a ser incluído na URL como parâmetro
            id = id_tranca

            # Formatando a URL com o ID como parâmetro
            url = f'{base_url}/{id}'

# Fazendo a requisição GET
            response_tranca = requests.get(url)
    # Passo 1: Validar o número da tranca
            response_tranca = requests.get(f'https://equipamento.onrender.com/tranca/{id_tranca}')
            if response_tranca.status_code == 200:
                # Passo 2: Ler o número da bicicleta presa na tranca
                numero_bicicleta = response_tranca.json().get('ID')
                print("num bicicleta:", numero_bicicleta)
                # Passo 3: Verificar se o ciclista pode alugar a bicicleta
                response_pode_alugar = Ciclista.pode_alugar(id_ciclista, numero_bicicleta)
                if response_pode_alugar['status'] == 200:
                    # Passo 4: Enviar cobrança
                    response_cobranca = requests.post('https://externo-vadebike.onrender.com/cobranca',
                                                    json={"ciclista": id_ciclista, "valor": valor_cobranca})
                    print("resp cpbranca:", response_cobranca)
                    if response_cobranca.status_code == 200:
                        # Passo 5: Registrar dados da retirada da bicicleta
                        response_registro_retirada = Ciclista.dados_retirada(id_ciclista, numero_bicicleta)
                        print(response_registro_retirada)
                            # Passo 6: Alterar status da bicicleta para "EM_USO"
                        """
                        acao = 'EM_USO'
                        response_alterar_status_bicicleta = requests.post(f'https://equipamento.onrender.com/bicicleta/{numero_bicicleta}/status/{acao}',
                                                                          json={"id": numero_bicicleta, "status": acao})
                                                                            
                        print(response_alterar_status_bicicleta)
                        if response_alterar_status_bicicleta.status_code == 200:
                        """
                                # Passo 7: Enviar email
                        response_enviar_email = requests.post("https://externo-vadebike.onrender.com/enviaremail", {
                            'assunto': 'vaDeBike',
                            'email': email,
                            'mensagem': 'Sua bicicleta está pronta para uso!'
                        })
                        #if response_enviar_email.status_code == 200:
                        return f"Bicicleta alugada com sucesso." + Ciclista.dados_retirada(id_ciclista, numero_bicicleta)
            
                    # Em caso de falha em qualquer passo, retorna uma mensagem de erro.
                    return "Falha ao alugar a bicicleta."


    def pode_alugar(id_ciclista, numero_bicicleta):

        return {'status': 200, 'id': 'xyz'}

    def dados_retirada(id_ciclista, numero_bicicleta):
        horario_retirada = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        return {"id_ciclista": id_ciclista, "id_bicicleta": numero_bicicleta, "horario_retirada": horario_retirada}

    @classmethod
    def devolve_bicicleta(cls, ciclista_id):
        for c in cls.ciclistas:
            if c.id == ciclista_id and c.bicicleta_alugada:
                bicicleta_devolvida = c.bicicleta_alugada
                c.bicicleta_alugada = None
                return f"Bicicleta devolvida com sucesso. Detalhes da Bicicleta Devolvida"
               
        return "Ciclista não encontrado ou não possui uma bicicleta alugada"    
        
    # Criar instâncias de Passaporte e MeioDePagamento para os ciclistas
    @staticmethod
    def iniciar():
        passaporte1 = Passaporte(numero="123456789", validade="01/25", pais="Brasil")
        meio_pagamento1 = MeioDePagamento(nome_titular="Nome1", numero="1111222233334444", validade="12/25", cvv="123")

        passaporte2 = Passaporte(numero="987654321", validade="02/26", pais="Estados Unidos")
        meio_pagamento2 = MeioDePagamento(nome_titular="Nome2", numero="5555666677778888", validade="01/26", cvv="456")

        passaporte3 = Passaporte(numero="456789123", validade="03/27", pais="Canadá")
        meio_pagamento3 = MeioDePagamento(nome_titular="Nome3", numero="9999000011112222", validade="02/27", cvv="789")

        # Criar ciclistas e adicioná-los à lista
        ciclista1 = Ciclista(
            id="1",
            nome="Ciclista1",
            nascimento="01/01/1990",
            cpf="123.456.789-01",
            passaporte=passaporte1,
            nacionalidade="Brasileira",
            email="ciclista1@email.com",
            url_foto_documento="url1",
            senha="senha1",
            meio_de_pagamento=meio_pagamento1
        )

        ciclista2 = Ciclista(
            id="2",
            nome="Ciclista2",
            nascimento="02/02/1991",
            cpf="987.654.321-02",
            passaporte=passaporte2,
            nacionalidade="Americana",
            email="ciclista2@email.com",
            url_foto_documento="url2",
            senha="senha2",
            meio_de_pagamento=meio_pagamento2
        )

        ciclista3 = Ciclista(
            id="3",
            nome="Ciclista3",
            nascimento="03/03/1992",
            cpf="456.789.123-03",
            passaporte=passaporte3,
            nacionalidade="Canadense",
            email="ciclista3@email.com",
            url_foto_documento="url3",
            senha="senha3",
            meio_de_pagamento=meio_pagamento3
        )
        Ciclista.ciclistas.extend([ciclista1, ciclista2, ciclista3])
    @staticmethod
    def imprime_ciclistas():
        headers = ["ID", "Nome", "Nascimento", "CPF", "Nacionalidade", "Email", "URL foto", "Ativo", "MeioPagamento"]
        data = [(c.id, c.nome, c.nascimento, c.cpf, c.nacionalidade, c.email, c.url_foto_documento, c.ativo, c.meio_de_pagamento) for c in Ciclista.ciclistas]

        table = tabulate(data, headers=headers, tablefmt="pretty")
        print(table)
