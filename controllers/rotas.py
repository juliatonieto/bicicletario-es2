import unittest
from flask import Flask, jsonify, render_template, request
import sys
import sys
sys.path.insert(0, 'C:/Users/juxto/OneDrive/Documentos/MICROSSERVICO_ALUGUEL/Aluguel')
from models.ciclista import Ciclista
from models.funcionario import Funcionario

app = Flask(__name__)

@app.route('/', methods=['GET'])
def rota_padrao():
    return "Bem-vindo à minha aplicação!"

@app.route('/restaurarDados', methods=['GET'])
def restaurar_dados():
    Ciclista.ciclistas = []
    return "Todos os dados foram restaurados para o estado inicial."

class TestRotas(unittest.TestCase):
    def setUp(self):
        app = Flask(__name__)
        self.client = app.test_client()

    def test_rota_padrao(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # Adicione mais verificações conforme necessário

    def test_restaurar_dados(self):
        response = self.client.get('/restaurarDados')
        self.assertEqual(response.status_code, 200)

@app.route('/ciclista',  methods = ['POST'])
def cadastrar_ciclista():
    try:
        # Obtém os dados do corpo da solicitação JSON
        data = request.get_json()

        # Cria uma instância da classe Ciclista com os dados fornecidos
        ciclista = Ciclista(**data)
        resposta = Ciclista.cadastra_ciclista(ciclista)
        # Chama o método estático de cadastro e retorna a resposta adequada
        return resposta
    except Exception as e:
        # Retorna a resposta adequada em caso de erro durante o processamento da solicitação
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/ciclista/<id>', methods=['GET'])
def recuperar_ciclista(id):
    try:
        resposta = Ciclista.retorna_ciclista(id)
        return resposta
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}
    
    
@app.route('/ciclista/existeEmail/<email>', methods=['GET'])
def buscar_email(email):
    try:
        return Ciclista.busca_email(email)
    except Exception as e:
        # Retorna a resposta adequada em caso de erro durante o processamento da solicitação
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/ciclista/<id>', methods=['PUT'])
def alterar_dados_ciclista(id):
    try:
        dados_atualizados = request.json 
        return Ciclista.altera_ciclista(id, **dados_atualizados)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/ciclista/<id>/ativar', methods=['POST'])
def ativar_ciclista(id):
    try:
        return  Ciclista.ativa_ciclista(id)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/ciclista/<id>/permiteAluguel', methods=['GET'])
def permitir_aluguel(id):
    try:
        return Ciclista.permite_aluguel(id)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/ciclista/<id>/bicicletaAlugada', methods=['GET'])
def mostrar_bicicleta_alugada(id):
    try:
        return Ciclista.bicicleta_alugada(id)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}

#------------------Funcionario-----------------------------

@app.route('/funcionario', methods=['GET'])
def mostrar_funcionarios():
    try:
        resposta = Funcionario.imprime_funcionarios()
        return resposta
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/funcionario', methods=['POST'])
def cadastrar_funcionario():
    try:
        data = request.get_json()
        funcionario = Funcionario(**data)

        return Funcionario.cadastra_funcionario(funcionario)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500

@app.route('/funcionario/<id>', methods=['GET'])
def recuperar_funcionario(id):
    try:
        resposta = Funcionario.retorna_funcionario(id)
        return resposta
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}

@app.route('/funcionario/<id>', methods=['PUT'])
def alterar_dados_funcionario(id):
    try:
        dados_atualizados = request.json
        return Funcionario.altera_funcionario(id, **dados_atualizados)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500

@app.route('/funcionario/<id>', methods=['DELETE'])
def deletar_funcionario(id):
    try:
        return Funcionario.remove_funcionario(id)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500

#------------------Cartão de crédito-----------------------------

@app.route('/CartaoDeCredito/<id>', methods=['GET'])
def recuperar_cc(id):
    try:
        return Ciclista.retorna_cc(id)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500


@app.route('/CartaoDeCredito/<id>', methods=['PUT'])
def alterar_cc(id):
    try:
        dados_atualizados = request.json
        return Ciclista.altera_cc(id, **dados_atualizados)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500

#------------------Aluguel-----------------------------

@app.route('/aluguel',  methods = ['POST'])
def realizar_aluguel():
    try:
        data = request.get_json()
        return Ciclista.aluga_bicicleta(**data)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500

#------------------Devolução-----------------------------

@app.route('/devolucao/<id>',  methods = ['POST'])
def realizar_devolucao(id):
    try:
        return Ciclista.devolve_bicicleta(id)
    except Exception as e:
        return {"codigo": "500", "mensagem": str(e)}, 500

if __name__ == '__main__':
    Ciclista.iniciar()
    Ciclista.imprime_ciclistas()
    app.run()
