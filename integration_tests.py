import unittest
from unittest.mock import MagicMock
from flask import Flask
from flask.testing import FlaskClient
import requests
from models.ciclista import Ciclista, MeioDePagamento, Passaporte
from models.funcionario import Funcionario
from controllers.rotas import app

class TestIntegracaoCiclista(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.client = app.test_client()
        Ciclista.iniciar()

    def test_cadastrar_ciclista(self):
        # Testar o cadastro de um ciclista
        novo_ciclista = Ciclista(
            nome="Novo Ciclista",
            nascimento="01/01/1990",
            cpf="123.456.789-01",
            passaporte=Passaporte(numero="123456789", validade="01/25", pais="Brasil"),
            nacionalidade="Brasileira",
            email="novo_ciclista@email.com",
            url_foto_documento="url_novo",
            senha="senha_novo",
            meio_de_pagamento=MeioDePagamento(nome_titular="Titular", numero="1234567812345678", validade="01/25", cvv="123")
        )

        response = self.client.post('/cadastrar_ciclista', json=novo_ciclista.__dict__)
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(data.get('id'))

    def test_cadastrar_ciclista_duplicado(self):
        # Testar o cadastro de um ciclista com e-mail duplicado
        ciclista_existente = Ciclista.ciclistas[0]
        ciclista_existente.email = "ciclista_existente@email.com"

        response = self.client.post('/cadastrar_ciclista', json=ciclista_existente.__dict__)
        data = response.get_json()

        self.assertEqual(response.status_code, 400)
        self.assertEqual(data.get('codigo'), "E001")

    def test_alugar_bicicleta(self):
        # Testar o processo de aluguel de bicicleta
        ciclista = Ciclista.ciclistas[0]
        ciclista.ativo = True

        response = self.client.post('/alugar_bicicleta', json={"id_tranca": 123, "id_ciclista": ciclista.id, "valor_cobranca": 10.0, "email": ciclista.email})
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertIn("Bicicleta alugada com sucesso.", data)

    def test_devolver_bicicleta(self):
        # Testar o processo de devolução de bicicleta
        ciclista = Ciclista.ciclistas[0]
        ciclista.bicicleta_alugada = 1

        response = self.client.post('/devolver_bicicleta', json={"id_ciclista": ciclista.id})
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertIn("Bicicleta devolvida com sucesso.", data)

    def test_devolver_bicicleta_ciclista_inexistente(self):
        # Testar o processo de devolução de bicicleta com ID de ciclista inexistente
        response = self.client.post('/devolver_bicicleta', json={"id_ciclista": "ciclista_inexistente"})
        data = response.get_json()

        self.assertEqual(response.status_code, 400)
        self.assertIn("Ciclista não encontrado ou não possui uma bicicleta alugada", data)
        
    
    def test_envia_email_sucesso(self, mock_post):
        # Configurar o mock para simular uma resposta bem-sucedida
        mock_post.return_value.status_code = 200

        # Executar o método de teste
        resultado = Ciclista.envia_email(self.email, self.mensagem)

        # Verificar os resultados
        self.assertEqual(resultado, "E-mail enviado.")

    
    def test_envia_email_falha(self, mock_post):
        # Configurar o mock para simular uma falha na resposta
        mock_post.return_value.status_code = 500

        # Executar o método de teste
        resultado = Ciclista.envia_email(self.email, self.mensagem)

        # Verificar os resultados
        self.assertEqual(resultado, "Não foi possível enviar o e-mail.")

    
    def test_valida_cc_sucesso(self, mock_post):
        # Configurar o mock para simular uma resposta bem-sucedida
        mock_post.return_value.status_code = 200

        # Executar o método de teste
        resultado = Ciclista.valida_cc(self.meio_de_pagamento)

        # Verificar os resultados
        self.assertEqual(resultado, "Cartão validado")

    
    def test_valida_cc_falha(self, mock_post):
        # Configurar o mock para simular uma falha na resposta
        mock_post.return_value.status_code = 500

        # Executar o método de teste
        resultado = Ciclista.valida_cc(self.meio_de_pagamento)

        # Verificar os resultados
        self.assertEqual(resultado, "Cartão recusado ou inválido")

    
    def test_ativa_ciclista_sucesso(self, mock_post):
        # Configurar o mock para simular uma resposta bem-sucedida
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = {
            "id": "123",
            "nome": "Teste",
            "nascimento": "01/01/2000",
            "cpf": "12345678900",
            "passaporte": "ABC123",
            "nacionalidade": "Brasileira",
            "email": "test@example.com",
            "url_foto_documento": "https://example.com/photo.jpg",
            "meio_de_pagamento": {}
        }

        # Executar o método de teste
        resultado = Ciclista.ativa_ciclista(self.id_ciclista)

        # Verificar os resultados
        self.assertEqual(resultado["id"], "123")
        self.assertEqual(resultado["nome"], "Teste")
        # Adicione mais verificações conforme necessário

    
    def test_ativa_ciclista_falha(self, mock_post):
        # Configurar o mock para simular uma falha na resposta
        mock_post.return_value.status_code = 500

        # Executar o método de teste
        resultado = Ciclista.ativa_ciclista(self.id_ciclista)

        # Verificar os resultados
        self.assertEqual(resultado, {"codigo": "500", "mensagem": "Falha ao ativar o ciclista."})

    
    def test_retorna_ciclista_sucesso(self, mock_post):
        # Configurar o mock para simular uma resposta bem-sucedida
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = {
            "id": "123",
            "nome": "Teste",
            "nascimento": "01/01/2000",
            "cpf": "12345678900",
            "passaporte": "ABC123",
            "nacionalidade": "Brasileira",
            "email": "test@example.com",
            "url_foto_documento": "https://example.com/photo.jpg"
        }

        # Executar o método de teste
        resultado = Ciclista.retorna_ciclista(self.id_ciclista)

        # Verificar os resultados
        self.assertEqual(resultado["id"], "123")
        self.assertEqual(resultado["nome"], "Teste")
        # Adicione mais verificações conforme necessário

    
    def test_retorna_ciclista_falha(self, mock_post):
        # Configurar o mock para simular uma falha na resposta
        mock_post.return_value.status_code = 404

        # Executar o método de teste
        resultado = Ciclista.retorna_ciclista(self.id_ciclista)

        # Verificar os resultados
        self.assertEqual(resultado, {"codigo": "404", "mensagem": "Ciclista não encontrado."})

    class TestIntegracaoFuncionario(unittest.TestCase):
        def setUp(self):
            self.app = app
            self.client = app.test_client()
            Funcionario.funcionarios = []  # Limpa a lista de funcionários antes de cada teste

        def test_cadastro_funcionario(self):
            # Dados de exemplo para o teste
            dados_funcionario = {
                "email": "novo_funcionario@email.com",
                "senha": "senha",
                "confirmacao_senha": "senha",
                "nome": "Novo Funcionário",
                "idade": 30,
                "cpf": "987.654.321-02",
                "funcao": "Atendente"
            }

            # Fazendo a solicitação de cadastro
            response = self.client.post('/funcionario', json=dados_funcionario)

            # Verifica se a resposta é bem-sucedida (código 200)
            self.assertEqual(response.status_code, 200)

            # Verifica se o funcionário foi cadastrado corretamente
            self.assertEqual(len(Funcionario.funcionarios), 1)
            funcionario_cadastrado = Funcionario.funcionarios[0]
            self.assertEqual(funcionario_cadastrado.nome, "Novo Funcionário")
            
    class TestIntegracaoEnvioEmail(unittest.TestCase):
        def setUp(self):
            # Configurar o aplicativo Flask para teste
            self.app = app
            self.client = app.test_client()
            Ciclista.iniciar()

        def test_enviar_email_sucesso(self):
            # Testar o envio de e-mail com sucesso
            ciclista = Ciclista.ciclistas[0]

            response = self.client.post('/enviar_email', json={"assunto": "Assunto Teste", "email": ciclista.email, "mensagem": "Mensagem de teste"})
            data = response.get_json()

            self.assertEqual(response.status_code, 200)
            self.assertIn("E-mail enviado.", data)

        def test_enviar_email_falha(self):
            # Testar a falha no envio de e-mail (por exemplo, e-mail inválido)
            response = self.client.post('/enviar_email', json={"assunto": "Assunto Teste", "email": "email_invalido", "mensagem": "Mensagem de teste"})
            data = response.get_json()

            self.assertEqual(response.status_code, 400)
            self.assertIn("Não foi possível enviar o e-mail.", data)
        
    class TestIntegracaoAluguel(unittest.TestCase):
        def setUp(self):
            self.app = app
            self.client = app.test_client()
            Ciclista.iniciar()
        
    
    def test_aluga_bicicleta_sucesso(self, mock_get):
        # Configurar o mock para simular a resposta da tranca
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = {'ID': self.numero_bicicleta}

        # Configurar o mock para simular a resposta da verificação de aluguel
        Ciclista.pode_alugar = MagicMock(return_value={'status': 200, 'id': 'xyz'})

        # Configurar o mock para simular a resposta do pedido de cobrança
        requests.post.return_value.status_code = 200

        # Configurar o mock para simular a resposta do registro de retirada
        Ciclista.dados_retirada = MagicMock(return_value="Dados de retirada")

        # Executar o método de teste
        resultado = Ciclista.aluga_bicicleta(self.id_tranca, self.id_ciclista, self.valor_cobranca, self.email)

        # Verificar os resultados
        self.assertEqual(resultado, "Bicicleta alugada com sucesso. Dados de retirada")

    
    def test_aluga_bicicleta_falha_tranca(self, mock_get):
        # Configurar o mock para simular uma falha na resposta da tranca
        mock_get.return_value.status_code = 404

        # Executar o método de teste
        resultado = Ciclista.aluga_bicicleta(self.id_tranca, self.id_ciclista, self.valor_cobranca, self.email)

        # Verificar os resultados
        self.assertEqual(resultado, "Falha ao alugar a bicicleta.")

        def test_aluguel_bicicleta(self):
            # Dados de exemplo para o teste
            dados_aluguel = {
                "id_tranca": "123",
                "id_ciclista": "1",
                "valor_cobranca": 10.0,
                "email": "ciclista1@email.com"
            }

            # Fazendo a solicitação de aluguel
            response = self.client.post('/aluguel', json=dados_aluguel)

            # Verifica se a resposta é bem-sucedida (código 200)
            self.assertEqual(response.status_code, 200)

            # Verifica se o processo de aluguel foi concluído corretamente
            ciclista_aluguel = Ciclista.ciclistas[0]
            self.assertEqual(ciclista_aluguel.bicicleta_alugada, True)
            # Adicione mais verificações conforme necessário
            
        def test_aluguel_bicicleta_nao_devolvida(self):
            # Cenário: Tentativa de aluguel quando a bicicleta não foi devolvida
            dados_ciclista = {
                "nome": "Ciclista Teste",
                "nascimento": "01/01/1990",
                "cpf": "123.456.789-01",
                "passaporte": {"numero": "123", "validade": "01/25", "pais": "Brasil"},
                "nacionalidade": "Brasileira",
                "email": "ciclista_teste@email.com",
                "url_foto_documento": "url_foto",
                "senha": "senha",
                "meio_de_pagamento": {"nome_titular": "Nome", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"}
            }
            self.client.post('/ciclista', json=dados_ciclista)

            dados_aluguel = {
                "id_tranca": "123",
                "id_ciclista": "1",
                "valor_cobranca": 10.0,
                "email": "ciclista_teste@email.com"
            }
            response_aluguel = self.client.post('/aluguel', json=dados_aluguel)
            self.assertEqual(response_aluguel.status_code, 200)

            # Tentativa de novo aluguel com a bicicleta ainda em uso
            novo_dados_aluguel = {
                "id_tranca": "456",
                "id_ciclista": "1",
                "valor_cobranca": 15.0,
                "email": "ciclista_teste@email.com"
            }
            response_novo_aluguel = self.client.post('/aluguel', json=novo_dados_aluguel)
            self.assertEqual(response_novo_aluguel.status_code, 500)  # Deve falhar, pois a bicicleta não foi devolvida
            
    def test_aluguel_sucesso(self):
        # Cenário: Aluguel bem-sucedido
        dados_ciclista = {
            "nome": "Ciclista Teste",
            "nascimento": "01/01/1990",
            "cpf": "123.456.789-01",
            "passaporte": {"numero": "123", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "ciclista_teste@email.com",
            "url_foto_documento": "url_foto",
            "senha": "senha",
            "meio_de_pagamento": {"nome_titular": "Nome", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"}
        }
        self.client.post('/ciclista', json=dados_ciclista)

        dados_aluguel = {
            "id_tranca": "123",
            "id_ciclista": "1",
            "valor_cobranca": 10.0,
            "email": "ciclista_teste@email.com"
        }
        response = self.client.post('/aluguel', json=dados_aluguel)
        self.assertEqual(response.status_code, 200)
        # Adicione mais verificações conforme necessário

    def test_aluguel_validacao_cartao_falha(self):
        # Cenário: Falha na validação do cartão de crédito
        dados_ciclista = {
            "nome": "Ciclista Teste",
            "nascimento": "01/01/1990",
            "cpf": "123.456.789-01",
            "passaporte": {"numero": "123", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "ciclista_teste@email.com",
            "url_foto_documento": "url_foto",
            "senha": "senha",
            "meio_de_pagamento": {"nome_titular": "Nome", "numero": "1111222233334444", "validade": "12/25", "cvv": "000"}
        }
        self.client.post('/ciclista', json=dados_ciclista)

        dados_aluguel = {
            "id_tranca": "123",
            "id_ciclista": "1",
            "valor_cobranca": 10.0,
            "email": "ciclista_teste@email.com"
        }
        response = self.client.post('/aluguel', json=dados_aluguel)
        self.assertEqual(response.status_code, 500)
    

    def test_aluguel_obtencao_tranca_falha(self):
        # Cenário: Falha na obtenção da tranca
        dados_ciclista = {
            "nome": "Ciclista Teste",
            "nascimento": "01/01/1990",
            "cpf": "123.456.789-01",
            "passaporte": {"numero": "123", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "ciclista_teste@email.com",
            "url_foto_documento": "url_foto",
            "senha": "senha",
            "meio_de_pagamento": {"nome_titular": "Nome", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"}
        }
        self.client.post('/ciclista', json=dados_ciclista)

        dados_aluguel = {
            "id_tranca": "tranca_inexistente",
            "id_ciclista": "1",
            "valor_cobranca": 10.0,
            "email": "ciclista_teste@email.com"
        }
        response = self.client.post('/aluguel', json=dados_aluguel)
        self.assertEqual(response.status_code, 500)
        
        
class TestRotas(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.client = app.test_client()
        Ciclista.ciclistas = []
        Funcionario.funcionarios = []

    def test_rota_padrao(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Bem-vindo à minha aplicação!")

    def test_restaurar_dados(self):
        response = self.client.get('/restaurarDados')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Todos os dados foram restaurados para o estado inicial.")

    def test_cadastrar_ciclista(self):
        novo_ciclista = {
            "nome": "Novo Ciclista",
            "nascimento": "01/01/1990",
            "cpf": "123.456.789-01",
            "passaporte": {"numero": "123456789", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "novociclista@example.com",
            "url_foto_documento": "url",
            "senha": "senha123",
            "meio_de_pagamento": {"nome_titular": "Nome1", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"},
            "ativo": False
        }

        response = self.client.post('/ciclista', json=novo_ciclista)
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertIn("id", data)
        self.assertEqual(data["nome"], novo_ciclista["nome"])

    def test_recuperar_ciclista(self):
        # Primeiro, cadastra um novo ciclista
        novo_ciclista = {
            "nome": "Ciclista Teste",
            "nascimento": "01/01/1990",
            "cpf": "987.654.321-09",
            "passaporte": {"numero": "987654321", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "ciclistateste@example.com",
            "url_foto_documento": "url",
            "senha": "senha123",
            "meio_de_pagamento": {"nome_titular": "Nome1", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"},
            "ativo": False
        }
        response_cadastro = self.client.post('/ciclista', json=novo_ciclista)
        data_cadastro = response_cadastro.get_json()

        # Agora, tenta recuperar o ciclista
        response_recuperacao = self.client.get(f'/ciclista/{data_cadastro["id"]}')
        data_recuperacao = response_recuperacao.get_json()

        self.assertEqual(response_recuperacao.status_code, 200)
        self.assertEqual(data_recuperacao["nome"], novo_ciclista["nome"])

    def test_buscar_email_ciclista(self):
        # Primeiro, cadastra um novo ciclista
        novo_ciclista = {
            "nome": "Ciclista Teste",
            "nascimento": "01/01/1990",
            "cpf": "987.654.321-09",
            "passaporte": {"numero": "987654321", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "ciclistateste@example.com",
            "url_foto_documento": "url",
            "senha": "senha123",
            "meio_de_pagamento": {"nome_titular": "Nome1", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"},
            "ativo": False
        }
        response_cadastro = self.client.post('/ciclista', json=novo_ciclista)
        data_cadastro = response_cadastro.get_json()

        # Agora, tenta buscar o email do ciclista
        response_busca_email = self.client.get(f'/ciclista/existeEmail/{novo_ciclista["email"]}')
        data_busca_email = response_busca_email.get_json()

        self.assertEqual(response_busca_email.status_code, 200)
        self.assertTrue(data_busca_email)

    def test_alterar_ciclista(self):
        # Primeiro, cadastra um novo ciclista
        novo_ciclista = {
            "nome": "Ciclista Teste",
            "nascimento": "01/01/1990",
            "cpf": "987.654.321-09",
            "passaporte": {"numero": "987654321", "validade": "01/25", "pais": "Brasil"},
            "nacionalidade": "Brasileira",
            "email": "ciclistateste@example.com",
            "url_foto_documento": "url",
            "senha": "senha123",
            "meio_de_pagamento": {"nome_titular": "Nome1", "numero": "1111222233334444", "validade": "12/25", "cvv": "123"},
            "ativo": False
        }
        response_cadastro = self.client.post('/ciclista', json=novo_ciclista)
        data_cadastro = response_cadastro.get_json()

        # Agora, tenta alterar os dados do ciclista
        dados_atualizados = {
            "nome": "Novo Nome",
            "idade": 30
        }
        response_alteracao = self.client.put(f'/ciclista/{data_cadastro["id"]}', json=dados_atualizados)
        data_alteracao = response_alteracao.get_json()

        self.assertEqual(response_alteracao.status_code, 200)
        self.assertEqual(data_alteracao["nome"], dados_atualizados["nome"])
        self.assertEqual(data_alteracao["idade"], dados_atualizados["idade"])
    def test_mostrar_funcionarios(self):
        response = self.client.get('/funcionario')
        self.assertEqual(response.status_code, 200)

    def test_cadastrar_funcionario(self):
        novo_funcionario = {
            "nome": "Novo Funcionario",
            "idade": 25,
            "cpf": "123.456.789-01",
            "funcao": "Analista",
            "email": "novofuncionario@example.com",
            "senha": "senha123"
        }

        response = self.client.post('/funcionario', json=novo_funcionario)
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertIn("id", data)
        self.assertEqual(data["nome"], novo_funcionario["nome"])

    def test_recuperar_funcionario(self):
        # Primeiro, cadastra um novo funcionário
        novo_funcionario = {
            "nome": "Funcionario Teste",
            "idade": 30,
            "cpf": "987.654.321-09",
            "funcao": "Desenvolvedor",
            "email": "funcionarioteste@example.com",
            "senha": "senha456"
        }
        response_cadastro = self.client.post('/funcionario', json=novo_funcionario)
        data_cadastro = response_cadastro.get_json()

        # Agora, tenta recuperar o funcionário
        response_recuperacao = self.client.get(f'/funcionario/{data_cadastro["id"]}')
        data_recuperacao = response_recuperacao.get_json()

        self.assertEqual(response_recuperacao.status_code, 200)
        self.assertEqual(data_recuperacao["nome"], novo_funcionario["nome"])

    def test_alterar_dados_funcionario(self):
        # Primeiro, cadastra um novo funcionário
        novo_funcionario = {
            "nome": "Funcionario Teste",
            "idade": 30,
            "cpf": "987.654.321-09",
            "funcao": "Desenvolvedor",
            "email": "funcionarioteste@example.com",
            "senha": "senha456"
        }
        response_cadastro = self.client.post('/funcionario', json=novo_funcionario)
        data_cadastro = response_cadastro.get_json()

        # Agora, tenta alterar os dados do funcionário
        dados_atualizados = {
            "nome": "Novo Nome",
            "idade": 35
        }
        response_alteracao = self.client.put(f'/funcionario/{data_cadastro["id"]}', json=dados_atualizados)
        data_alteracao = response_alteracao.get_json()

        self.assertEqual(response_alteracao.status_code, 200)
        self.assertEqual(data_alteracao["nome"], dados_atualizados["nome"])
        self.assertEqual(data_alteracao["idade"], dados_atualizados["idade"])

    def test_deletar_funcionario(self):
        # Primeiro, cadastra um novo funcionário
        novo_funcionario = {
            "nome": "Funcionario Teste",
            "idade": 30,
            "cpf": "987.654.321-09",
            "funcao": "Desenvolvedor",
            "email": "funcionarioteste@example.com",
            "senha": "senha456"
        }
        response_cadastro = self.client.post('/funcionario', json=novo_funcionario)
        data_cadastro = response_cadastro.get_json()

        # Agora, tenta deletar o funcionário
        response_delecao = self.client.delete(f'/funcionario/{data_cadastro["id"]}')
        data_delecao = response_delecao.get_json()

        self.assertEqual(response_delecao.status_code, 200)
        self.assertEqual(data_delecao, "Dados removidos")


if __name__ == '__main__':
    unittest.main()




